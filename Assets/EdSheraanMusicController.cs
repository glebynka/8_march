﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class EdSheraanMusicController : MonoBehaviour
{
    [SerializeField] private List<GameObject> _rabbits = new List<GameObject>();

    [SerializeField] private List<GameObject> _trees = new List<GameObject>();

    private Coroutine _musicCoroutine;

    public static Action<bool> OnChorusStarted = (lastChore) => { };

    private IEnumerator MusicEventsIEnumerator()
    {
        Time.timeScale = 1;

        var verse1 = new[] {22, 30, 38, 48, 64, 66, 68, 70, 72, 74, 76, 78};

        var chorus1 = new[] {80, 82, 84, 86, 88, 90, 92, 94, 96, 98, 112, 120, 128, 142};

        var verse2 = new[] {151, 159, 167, 176, 178, 180, 182, 201, 202, 204, 205, 208};

        var chorus2 = new[] {210, 212, 214, 216, 218, 220, 222, 224, 226, 228, 240, 248, 256, 273};

        var chorus3 = new[] {306, 308, 310, 312, 314, 316, 318, 320, 322, 324, 339, 346, 354, 372};

        for (int i = 0; i < 375; i++)
        {
            Debug.Log($"TICK [{i}]");

            yield return StartCoroutine(OneTick());

            Verse(i, verse1);

            Сhorus(i, chorus1);

            Verse(i, verse2);

            Сhorus(i, chorus2);

            Bridge(i);

            Сhorus(i, chorus3);
        }

        _musicCoroutine = null;

        Time.timeScale = 0;
    }

    private void Сhorus(int i, int[] chorusOrder)
    {
        if (i == chorusOrder[0])
        {
            ActivateRabbit(0, true);
            ActivateRabbit(1, true);

            var lastChore = i >= 306;

            OnChorusStarted?.Invoke(lastChore);
        }

        if (i == chorusOrder[1])
        {
            ActivateTree(17, true);
        }

        if (i == chorusOrder[2])
        {
            ActivateTree(18, true);
        }

        if (i == chorusOrder[3])
        {
            ActivateTree(19, true);
        }

        if (i == chorusOrder[4])
        {
            ActivateTree(20, true);
        }

        if (i == chorusOrder[5])
        {
            ActivateTree(0, true);
        }

        if (i == chorusOrder[6])
        {
            ActivateTree(1, true);
        }

        if (i == chorusOrder[7])
        {
            ActivateTree(2, true);
        }

        if (i == chorusOrder[8])
        {
            ActivateTree(3, true);
        }

        if (i == chorusOrder[9])
        {
            ActivateTree(4, true);
        }

        if (i == chorusOrder[10])
        {
            UAUAUAUA(2, 3);
            UAUAUAUA(3, 3);
        }

        if (i == chorusOrder[11])
        {
            UAUAUAUA(2, 3);
            UAUAUAUA(3, 3);
        }

        if (i == chorusOrder[12])
        {
            UAUAUAUA(2, 3);
            UAUAUAUA(3, 3);
        }

        if (i == chorusOrder[13])
        {
            for (int j = 0; j < _rabbits.Count; j++)
            {
                ActivateRabbit(j, false);
            }

            for (int j = 0; j < _trees.Count; j++)
            {
                ActivateTree(j, false);
            }
        }
    }

    private void Verse(int i, int[] verseOrder)
    {
        if (i == verseOrder[0])
        {
            IUMMM(0);
        }

        if (i == verseOrder[1])
        {
            IUMMM(1);
        }

        if (i == verseOrder[2])
        {
            IUMMM(0);
        }

        if (i == verseOrder[3])
        {
            ActivateRabbit(0, true);
        }

        if (i == verseOrder[4])
        {
            ActivateRabbit(1, true);
        }

        if (i == verseOrder[5])
        {
            ActivateTree(0, true);
        }

        if (i == verseOrder[6])
        {
            ActivateTree(1, true);
        }

        if (i == verseOrder[7])
        {
            ActivateTree(2, true);
        }

        if (i == verseOrder[8])
        {
            ActivateTree(3, true);
        }

        if (i == verseOrder[9])
        {
            ActivateTree(4, true);
        }

        if (i == verseOrder[10])
        {
            for (int j = 0; j < 5; j++)
            {
                _trees[j].SetActive(false);
            }

            ActivateRabbit(0, false);
            ActivateRabbit(1, false);
        }
    }

    private void Bridge(int i)
    {
        if (i >= 275 && i <= 330)
        {
            if (i % 4 == 0)
            {
                Debug.Log($"Come on, be my baby, come on");
            }
        }
    }

    private void ActivateTree(int treeIndex, bool isActivate)
    {
        _trees[treeIndex].SetActive(isActivate);
    }

    private IEnumerator OneTick()
    {
        yield return new WaitForSeconds(0.615f);
    }

    private async void IUMMM(int bunnyIndex)
    {
        _rabbits[bunnyIndex].SetActive(true);

        await WaitAsynchForSeconds(1.5f);

        _rabbits[bunnyIndex].SetActive(false);
    }

    private async void UAUAUAUA(int bunnyIndex, float delayTime)
    {
        _rabbits[bunnyIndex].SetActive(true);

        for (int i = 5; i < 17; i++)
        {
            _trees[i].gameObject.SetActive(true);
        }

        for (int i = 22; i < 34; i++)
        {
            _trees[i].gameObject.SetActive(true);
        }

        await WaitAsynchForSeconds(delayTime);

        for (int i = 5; i < 17; i++)
        {
            _trees[i].gameObject.SetActive(false);
        }

        for (int i = 22; i < 34; i++)
        {
            _trees[i].gameObject.SetActive(false);
        }

        _rabbits[bunnyIndex].SetActive(false);
    }

    private void ActivateRabbit(int bunnyIndex, bool isActivate)
    {
        _rabbits[bunnyIndex].SetActive(isActivate);
    }

    private static async Task WaitAsynchForSeconds(float maxTime)
    {
        float time = 0;

        while (time <= maxTime)
        {
            time += Time.deltaTime;

            await Task.Yield();
        }
    }


    private void Awake()
    {
        if (_musicCoroutine == null)
        {
            _musicCoroutine = StartCoroutine(MusicEventsIEnumerator());
        }
    }
}