﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class TreeDance : MonoBehaviour
{
    [SerializeField] private float _jumpValue = 1;

    [SerializeField] private float _duration = 0.25f;

    [SerializeField] private Ease _startEase = Ease.Flash;

    [SerializeField] private Ease _endEase = Ease.Flash;

    private float _startLocalScaleZ;

    private Sequence _treeSequence = null;

    private void Start()
    {
        _startLocalScaleZ = transform.localScale.z;

        JumpDance();
    }

    public void JumpDance()
    {
        _treeSequence = DOTween.Sequence();
        _treeSequence.Append(JumpTweener());
        _treeSequence.Play();
    }

    public void StopDance()
    {
        Debug.Log($"Stop dance tree: {gameObject.name}");
        
        _treeSequence.Kill(true);
        _treeSequence = null;

        transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, _startLocalScaleZ);
    }

    private Tween JumpTweener()
    {
        return transform.DOScaleZ(_startLocalScaleZ * _jumpValue, _duration)
            .SetEase(_startEase)
            .OnComplete(() =>
            {
                transform
                    .DOScaleZ(_startLocalScaleZ, _duration)
                    .SetEase(_endEase)
                    .OnComplete(JumpDance);
            });
    }
}