﻿
using UnityEngine;

public class OptimazerFramerate : MonoBehaviour
{
    private void OnEnable()
    {
        Application.targetFrameRate = 60;
    }
}
