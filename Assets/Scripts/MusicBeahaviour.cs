﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicBeahaviour : MonoBehaviour
{
    public AudioClip targetClip;

    private void Start()
    {
        int bpm = UniBpmAnalyzer.AnalyzeBpm(targetClip);
        Debug.Log("BPM is " + bpm);
    }
}
