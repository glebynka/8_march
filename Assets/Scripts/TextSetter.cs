﻿using System.Collections;
using TMPro;
using UnityEngine;

public class TextSetter : MonoBehaviour
{
    [SerializeField] private Color _textColor;
    [SerializeField] private TextMeshProUGUI _songText;

    private Coroutine _setTextCoroutine = null;

    private string[] _chorusWords =
    {
        "I'm in love with the shape of you",
        "We push and pull like a magnet do",
        "Although my heart is falling too",
        "I'm in love with your body",
        "And last night you were in my room",
        "And now my bedsheets smell like you",
        "Every day discovering something brand new",
        "I'm in love with your body",
        "Oh—I—oh—I—oh—I—oh—I",
        "I'm in love with your body",
        "Oh—I—oh—I—oh—I—oh—I",
        "I'm in love with your body",
        "Oh—I—oh—I—oh—I—oh—I",
        "I'm in love with your body",
        "Every day discovering something brand new",
        "I'm in love with the shape of you"
    };

    private string[] _chorusWordsLast =
    {
        "I'm in love with the shape of you",
        "We push and pull like a magnet do",
        "Although my heart is falling too",
        "I'm in love with your body",
        "And last night you were in my room",
        "And now my bedsheets smell like you",
        "Every day discovering something brand new",
        "I'm in love with your body",
        "Come on, be my baby, come on",
        "I'm in love with your body",
        "Come on, be my baby, come on",
        "I'm in love with your body",
        "Come on, be my baby, come on",
        "I'm in love with your body",
        "Every day discovering something brand new",
        "I'm in love with the shape of you"
    };

    private void OnEnable()
    {
        EdSheraanMusicController.OnChorusStarted += SetText;
    }

    private void OnDisable()
    {
        EdSheraanMusicController.OnChorusStarted -= SetText;
    }

    private void SetText(bool lastChorus)
    {
        if (_setTextCoroutine == null)
        {
            _setTextCoroutine = StartCoroutine(SetTextIEnumerator(lastChorus));
        }
    }


    private IEnumerator SetTextIEnumerator(bool lastChorus)
    {
        var wordIndex = 0;

        var startText = _songText.text;
        var startTextColor = _songText.color;
        var startTextFontSize = _songText.fontSize;

        var chorus = !lastChorus ? _chorusWords : _chorusWordsLast;
        var count = lastChorus ? 76 : 64;
        
        for (int i = 0; i < count; i++)
        {
            if (i % 4 == 0)
            {
                _songText.color = _textColor;
                _songText.fontSize = 15f;
                _songText.text = chorus[wordIndex];

                wordIndex++;
            }

            yield return new WaitForSeconds(0.615f);
        }

        _songText.text = startText;
        _songText.color = startTextColor;
        _songText.fontSize = startTextFontSize;

        _setTextCoroutine = null;
    }
}