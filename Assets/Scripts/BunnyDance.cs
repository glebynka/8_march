﻿using System;
using DG.Tweening;
using UnityEngine;

public class BunnyDance : MonoBehaviour
{
    [SerializeField] private float _jumpValue = 1;

    [SerializeField] private float _duration = 0.25f;

    [SerializeField] private Ease _startEase = Ease.Flash;

    [SerializeField] private Ease _endEase = Ease.Flash;

    private float _startLocalScaleY;


    private void Awake()
    {
        _startLocalScaleY = transform.localScale.y;
        Jump();
    }

    private void Jump()
    {
        transform.DOScaleY(_startLocalScaleY * _jumpValue, _duration)
            .SetEase(_startEase)
            .OnComplete(() =>
            {
                transform
                    .DOScaleY(_startLocalScaleY, _duration)
                    .SetEase(_endEase)
                    .OnComplete(Jump);
            });
    }
}