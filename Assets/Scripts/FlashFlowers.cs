﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class FlashFlowers : MonoBehaviour
{
    [SerializeField] private List<GameObject> _flowers = new List<GameObject>();

    [SerializeField] private float _timerSpeed = 5.0f;

    private float _lastTimeStep;

    private void Start()
    {
        FLashRandomFlower();
    }

    private void OnEnable()
    {
        AnimationEventStateMachineBehaviour.OnAnimationEnded += FLashRandomFlower;
    }

    private void OnDisable()
    {
        AnimationEventStateMachineBehaviour.OnAnimationEnded -= FLashRandomFlower;
    }

    //private void Update()
   //{
   //    if (Time.time - _lastTimeStep >= _timerSpeed)
   //    {
   //        _lastTimeStep = Time.time;

   //        FLashRandomFlower();
   //    }
   //}

    private void FLashRandomFlower()
    {
        var random = Random.Range(0, _flowers.Count);

        foreach (var flower in _flowers)
        {
            flower.SetActive(false);
        }

        _flowers[random].SetActive(true);
    }
}